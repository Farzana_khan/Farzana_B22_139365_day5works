<?php

/*function($m)
{
    return $m*2;
}*/

$a=$b=5;
$c=$a++; //5
echo "c=";
echo $c;
echo "<hr>";

$e=$d=++$b; //6
echo "<hr> d=e=";
echo $e;
$f= ++$e;  //7
echo "<hr> f=";
echo $f;

$i=++$f+$f++; // left to right as same precedence
echo "<hr> i=";
echo $i; //16=8+8
echo "<br> last f=";
echo $f; //9

$j=$f++ + ++$f; //20=9+11  . left to right
echo "<hr> j="; //f=11 last
echo $j;
echo "<br> last f=";
echo $f;


$k=++$f - $f++ ; //  same precedence. left to right. 12-12
echo "<hr> k= ";
echo $k;
echo "<br> last f=";
echo $f;  //last f 13

$l=$f++ - $f--; //   12-13. right to left
echo "<hr> l=";
echo $l;
echo "<br> last f=";
echo $f;  //13

$m=--$f+ $f++; //
    //+ $f++; //   24 ?
echo "<hr> f=";
echo $f;
echo "<hr> m=";
echo $m;




