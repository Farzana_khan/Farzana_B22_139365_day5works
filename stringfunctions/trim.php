<?php
$text   = "\tThese are a few words :)...  ";
$hello  = "Hello World";
var_dump($text);
echo "<br>";
$trimmed = trim($text);
var_dump($trimmed);
echo "<br>";
$trimmed = trim($text, " \t.");
var_dump($trimmed);
echo "<br>";
$trimmed = trim($hello, "Hdle");
var_dump($trimmed);
