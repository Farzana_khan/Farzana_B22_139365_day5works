<?php
$text = "\t\tThese are a few words :) ...   ";
$binary = "\x09Example string\x0A";
$hello  = "Hello World";
var_dump($text);
echo "<br>";

var_dump($binary);
echo "<br>";
var_dump($hello);
echo "<br>";

print "\n";

$trimmed = rtrim($text);
var_dump($trimmed);

echo "<br>";
$trimmed = rtrim($text, " \t.");
var_dump($trimmed);